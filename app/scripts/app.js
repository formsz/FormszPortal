'use strict';

/**
 * @ngdoc overview
 * @name formszWebApp
 * @description
 * # formszWebApp
 *
 * Main module of the application.
 */
var formszWebApp = 
  angular.module('formszWebApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ui.router',
    'ui.bootstrap',
    'ngMaterial',
    'LocalStorageModule',
    'formszWebApp.constants',
    'formszWebApp.directives'
  ]);


  formszWebApp.config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise("/login");
      $stateProvider
      .state ('login', {
          url: '/login',
          templateUrl: 'views/login.html'
      })
      .state ('pages', {
          url: '/pages',
          templateUrl: 'views/common.html'
      })
      .state ('pages.admin', {
          url: '/admin',
          templateUrl: 'views/admins.html',
          controller:'adminCtrl'
      })
      .state ('pages.admin.view', {
          url: '/view',
          templateUrl: 'views/viewAdmins.html'
      })
      .state ('pages.admin.create', {
                url: '/create',
                templateUrl: 'views/createAdmin.html'
      })
      .state('pages.admin.edit', {
                url: '/edit',
                templateUrl: 'views/EditAdmin.html'
            })
      .state ('pages.usergroups', {
                url: '/usergroups',
                templateUrl: 'views/userGroup.html',
                controller:'usergroupCtrl'
      })
      .state ('pages.usergroups.view', {
        url: '/view',
        templateUrl: 'views/userGroupView.html'
      })
      .state ('pages.users', {
                url: '/users',
                templateUrl: 'views/users.html',
                controller:'userCtrl'
      })
      .state ('pages.users.view', {
          url: '/view',
          templateUrl: 'views/userView.html'
      })
      .state ('pages.users.create', {
          url: '/create',
          templateUrl: 'views/createUser.html'
      })
      .state('pages.users.edit', {
                url: '/edit',
                templateUrl: 'views/EditUser.html'
            })
     .state ('pages.formsz', {
              url: '/formsz',
              templateUrl: 'views/formsz.html',
              controller:'formszCtrl'
      })
     .state('pages.formsz.get', {
                url: '/get',
                templateUrl: 'views/getForms.html'
      })

     .state('pages.formsz.submitForm',{
            url: '/submitForm',
            templateUrl: 'views/formSumition.html'
     })
     
     .state('pages.formsz.create', {
                url: '/create',
                templateUrl: 'views/createForm.html'
      })
      .state('pages.formsz.records', {
                url: '/records',
                templateUrl: 'views/getRecords.html'
      })
      .state('pages.formsz.records.table', {
                url: '/table',
                templateUrl: 'views/tablerView.html',
                controller: 'formszCtrl'
      })
      .state('pages.formsz.grid', {
                url: '/gridView',
                templateUrl: 'views/gridViewRecord.html'
      })
     .state ('pages.formsz.formBuilder', {
              url: '/formBuilder',
              templateUrl: 'views/formBuilder.html'
      })
     .state ('pages.template', {
                url: '/template',
                templateUrl: 'views/templates.html',
                controller: 'formszCtrl'
      })
     .state('pages.template.get', {
                url: '/get',
                templateUrl: 'views/getTemplates.html'
      })
     .state('pages.template.forAdmins', {
                url: '/forAdmins',
                templateUrl: 'views/getTemplatesForAdmins.html'
      })
      .state ('pages.template.formBuilder', {
              url: '/formBuilder',
              templateUrl: 'views/formBuilder.html'
      })

     .state ('pages.profile', {
                url: '/profile',
                templateUrl: 'views/profile.html'
            })
  
      .state ('pages.profile.profile-about', {
          url: '/profile-about',
          templateUrl: 'views/profile-about.html'
      })
  
      .state ('pages.profile.profile-timeline', {
          url: '/profile-timeline',
          templateUrl: 'views/profile-timeline.html'
      })

      .state ('pages.profile.profile-photos', {
          url: '/profile-photos',
          templateUrl: 'views/profile-photos.html'
      })
  
      .state ('pages.profile.profile-connections', {
          url: '/profile-connections',
          templateUrl: 'views/profile-connections.html'
      })
      
    });

    formszWebApp.run(function($state,$rootScope){
    //$rootScope.$state = $state;
       // $rootScope.$state = $state;
     
  })