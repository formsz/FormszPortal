angular.module('formszWebApp.constants', [])
    .constant('CONSTANTS', (function() {
        // **** Development Server ***** //
        //var baseUrl         =  "http://192.168.110.219:3000";
        // var baseUrl       =  "http://183.82.100.86:4001/formsz";
        var baseUrl       =  "http://formsz-prdmm.rhcloud.com";
        var serverURL       =   baseUrl+"/api/v1";
        var unsecureUrl     =   baseUrl+"/api/v";
        return {
        
        Golobal_imagePath:unsecureUrl+"/",
        login:baseUrl+"/login",
        forgotPassword:baseUrl+"/forgotpwd",
        createUSer:serverURL+"/Users/create",
        createAdmin:serverURL+"/admins/create",
        getUsers:serverURL+"/users/getusergrouplist",
        getAdmin:serverURL+"/admins/getadminslist",
        pwdChange:serverURL+"/users/pwdchange",
        deleteAdmin:serverURL+"/admins",
        deleteUser:serverURL+"/Users",
        emailRecords:serverURL+"/formszDetails/download/",
        createGroup:serverURL+"/group/create",
        getUserGroup:serverURL+"/group/getgrouplist",
        getUnMappedAdminList:serverURL+"/admins/UMadminslist",        
        getUnMappedGroup:serverURL+"/group/UMgrouplist",
        UpdateAdmin:serverURL+"/admins/Update",
        UpdateUsers:serverURL+"/Users/Update",
        deleteUserGroup:serverURL+"/group/delete",
        Updategroup:serverURL+"/group/Update",
        getListOfFormsz:serverURL+"/formsz/getformszlist",
        deleteFormsz:serverURL+"/formsz/delete",
        getrecords:serverURL+"/formszDetails/getformszDetail",
        viewFormsDetail:serverURL+"/formsz",
        createForm:serverURL+"/formsz/create",
        submitForm:serverURL+"/formszDetails/create",
        getform:serverURL+"/formsz",
        deleteExistRecords:serverURL+"/deleteFormrecords",
        isFormExists:serverURL+"/formsz/isFormszexits",
        getUserGroupForAdmin:serverURL+"/admins",
        getUsersList:serverURL+"/users/getuserlistmaping",
        getAdminListForUserGroup :serverURL+"/group",
        getListOfTempaltes:serverURL+"/formsz/getallTemplates",
        ReassignRecord:serverURL+"/formszDetails/ReAssign",
        SECTION_SELECTED_SECTION:"Please select the section",
        SECTION_SELECTED_GROUP:"Please select the group",
        GROUP_SELECT_FORM_SECTION_ALERT:"Please seclect the section of this group",
        serverProblem:"Server Preoblem occured,Please contact adminstrator."
        }
    })());