angular.module('formszWebApp').service('restServices', function($http,localStorageService){
        var rs = {};
        rs.restPostType = function(url, data,config,cb) {
           rs.showLoader();
           $http.post(url, data,config).then(function(response){
              rs.removeLoader();
              cb(true,response);
           },
            function(response){
              rs.removeLoader();
              cb(false,"fail");
            })
        }
        
        rs.restPutType = function(url, data,config,cb) {
          rs.showLoader();
           $http.put(url, data,config).then(function(response){
              rs.removeLoader();
              cb(true,response);
           },
            function(response){
               rs.removeLoader();
               cb(false,"fail");
            })
        }

        rs.restPostTypeForForm = function(url, data,config,cb) {
          rs.showLoader();
          $http.post(url, data,config).then(function(response){
              rs.removeLoader();
              cb(true,response);
          },
          function(response){
              rs.removeLoader();
              cb(false,"fail");
          })
        }

        rs.restGetType = function(url,headers,cb){
          rs.showLoader();
            $http.get(url,headers).then(function(response){
              rs.removeLoader();
              cb(true,response);
            },
            function(response){
              rs.removeLoader();
              cb(false,response);
            })
        }
        rs.restDeleteType=function(url,config,cb){
           rs.showLoader();
           $http.delete(url, config).then(function(response){
            rs.removeLoader();
            cb(true,response);
           }, 
           function(response){
            rs.removeLoader();
            cb(false,response);
           })
        }
        rs.restPutType = function(url,data,config,cb){
           rs.showLoader();
            $http.put(url, data, config).then(function(response){
                rs.removeLoader();
                cb(true,response);
              }, 
              function(response){
                rs.removeLoader();
                cb(false,response);
              }
            );
      }
      rs.getHeaders=function(){
        var access_token = localStorageService.get("X-Access-Token");
        return {
                "Content-Type" : "application/json",
                "X-Access-Token" : access_token,
                "X-Key": "ajay"
             };
      }
      rs.getHeadersForPost = function(data,url){
            var access_token = localStorageService.get("X-Access-Token");
            return {
                "Content-Type" : undefined,
                "X-Access-Token"  : access_token ,
                "X-Key": "ajay"    
             };
      }
      rs.showLoader = function () {
        $('body').append('<div class="loader"></div>');
      }
      rs.removeLoader =function(){
        $('.loader').fadeOut(function () {
            $(this).remove();
        });   
      }
        return rs;
    })
    