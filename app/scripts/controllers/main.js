'use strict';

/**
 * @ngdoc function
 * @name formszWebApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the formszWebApp
 */
angular.module('formszWebApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  })
  .controller('materialadminCtrl', function($timeout, $state, $scope, growlService){
        //Welcome Message
        // growlService.growl('Welcome back Mallinda!', 'inverse')
        
        
        // Detact Mobile Browser
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           angular.element('html').addClass('ismobile');
        }

        // By default Sidbars are hidden in boxed layout and in wide layout only the right sidebar is hidden.
        this.sidebarToggle = {
            left: false,
            right: false
        }

        // By default template has a boxed layout
        this.layoutType = localStorage.getItem('ma-layout-status');
        
        // For Mainmenu Active Class
        this.$state = $state;    
        
        //Close sidebar on click
        this.sidebarStat = function(event) {
            if (!angular.element(event.target).parent().hasClass('active')) {
                this.sidebarToggle.left = false;
            }
        }
        
        //Listview Search (Check listview pages)
        this.listviewSearchStat = false;
        
        this.lvSearch = function() {
            this.listviewSearchStat = true; 
        }
        
        //Listview menu toggle in small screens
        this.lvMenuStat = false;
        
        //Blog
        this.wallCommenting = [];
        
        this.wallImage = false;
        this.wallVideo = false;
        this.wallLink = false;

        //Skin Switch
        this.currentSkin = 'bluegray';

        this.skinList = [
            'lightblue',
            'bluegray',
            'cyan',
            'teal',
            'green',
            'orange',
            'blue',
            'purple'
        ]

        this.skinSwitch = function (color) {
            this.currentSkin = color;
        }
    })

    .controller('sidebarCtrl', function($timeout,$scope,localStorageService,$rootScope,$state){
       $scope.topViewForms= function(){
            $rootScope.topViews= ["Home","Formsz"];
            if($rootScope.topViews.length>2){
                $rootScope.topViews.pop($rootScope.formname);
            }
            $state.go("pages.formsz.get");
        }

        $scope.type = localStorageService.get("userdata").type;
    })

    //=================================================
    // LOGIN
    //=================================================
    .controller('loginCtrl',function($rootScope, $scope, $state, restServices, localStorageService,CONSTANTS) {
    this.login = 1;
    this.register = 0;
    this.forgot = 0;
    $scope.invalidCrd = false;

    $scope.login = function(userData,isValid,status) {
         if(status==1)
        localStorageService.set("isLightView",true);
        else
        localStorageService.set("isLightView",false);

        if(isValid){
        var headers = restServices.getHeaders();
        var config = {};
        config.headers = headers;
        var url = CONSTANTS.login;
        restServices.restPostType(url,userData,config,function(status, data) {
        var admingroup = data.data.user;
        localStorageService.set("loginUser", admingroup);
            if (status) {
                if (data.data.status == 200) {
                    localStorageService.set("userdata",data.data.user);
                    localStorageService.set("X-Access-Token", data.data.token);
                    localStorageService.set("GroupName",data.data.user.groupname);
                    if (data.data.user.groupname != null) {
                        if(data.data.user.type == '0')
                            $state.go("pages.admin.view");
                        else if(data.data.user.type == '1')
                            $state.go("pages.users.view");
                        else if(data.data.user.type == '2')
                            $state.go("pages.formsz.get");
                    } else {
                        swal("Group should be assigned");
                    }
                }
                else if(data.data.status == 204){
                    $scope.invalidCrd = true;
                }

                 else {
                    swal(CONSTANTS.serverProblem);
                }
            }
        });
        }
        else{
            $scope.$broadcast('show-error-event');
        }
    }

    // RESET PASSWORD:
    $scope.reset = function(changePwd,isValid) {
        if(isValid){
            var headers = restServices.getHeaders();
            var config = {};
            config.headers = headers;
            var url = CONSTANTS.forgotPassword;
            restServices.restPostType(url,changePwd,config,function(status, data) {
                if (status) {
                    if (data.status == 200) {
                        swal(data.data.Message);
                        $state.go("login");
                    }
                    else if(data.status == 204){
                        swal(data.data.Message);
                    }
                    else {
                       swal(CONSTANTS.serverProblem);
                    }
                }
            });
        }
        else{
             $scope.$broadcast('show-error-event');
        }
    }
})
    

