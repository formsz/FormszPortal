'use strict';
var app = angular.module('formszWebApp').controller('userCtrl', ['$rootScope', '$uibModal', '$scope', '$window', '$state', '$http', 'CONSTANTS', 'restServices', 'localStorageService', function($rootScope, $uibModal, $scope, $window, $state, $http, CONSTANTS, restServices, localStorageService) {
    $scope.isClearImg= true;
    $scope.imgUrldata = CONSTANTS.Golobal_imagePath;
    $scope.defaultPage =1; // initialize page no to 1
    $scope.maxSize = 5;
    $scope.uploadImageRef = null;
    $scope.errSrc = "images/nopreview.jpg";
    $scope.topView_Users=["Home","Users"];
    $scope.userEdit ={};
    $scope.phoneValidation="^[0-9]{10}$";
    $scope.isImageChange =false;
    $scope.userFormConstants = {
        formHeader: "Create User",
        UserNameLabel: "UserName",
        nameLable: "Name",
        Email: "Email",
        Phone: "Phone",
        UNRAlert: "Username is required.",
        NRAlert: "name is required.",
        ERAlert: "Email is required.",
        EINAlert: "Invalid Email!",
        PNRAlert: "Phone number is required.",
        PNSAlert: "Phone number is too short!",
        IRAlert: "Please upload the image"
    }
    $scope.topView=["Home","Users"];
    $scope.topViewUsers= function(){
        if($scope.topView_Users.length>2){
            $scope.topView_Users.splice(2,1);
            $state.go("pages.users.view");
        }
    }
    $scope.create_user = function() {
        $scope.topView_Users.push("Create User"); 
        $scope.updateImage ="";
        $state.go('pages.users.create');
    }
    $scope.searchBarHide =true;
    $scope.closeFilter=function(){
        $scope.searchBy={};
        $scope.cssHeader= false;
        $scope.searchBarHide= true;
    }
    $scope.openFilter = function(){
        $scope.searchBy= null;
        $scope.searchBarHide= false;
        $scope.cssHeader= true;
    }
    $rootScope.topViewUsers= function(){
        if($scope.topView.length>2){
            $scope.topView.splice(2,1);
            $state.go("pages.users.view");
    }
    }
//****************Creating User*******************
    $scope.createUser = function(userData, isValid) {
        var userdata = userData;
        var loginadmin = localStorageService.get("loginUser");
        var groupname = localStorageService.get("GroupName");
        userdata['type'] = 2;
        userdata['admingroup'] = loginadmin;
        userdata['groupname'] = groupname;
        var headers = restServices.getHeadersForPost();
        var config = {};
        config.headers = headers;
        if (isValid) {
                $scope.formdata = new FormData();
                if( !$scope.isAccountImageSelected){
                    $scope.uploadImageRef = null;
                }
                else if($scope.isAccountImageSelected && !$scope.isAccountImageValid){
                    swal("Invalid Image, please choose correct Image");
                return;
                }
                $scope.formdata.append('img', $scope.uploadImageRef);
                $scope.formdata.append('data', JSON.stringify(userdata));
                var url = CONSTANTS.createUSer;
                restServices.restPostType(url, $scope.formdata, config, function(status, res) {
                    if (status) {
                        if (res.data.status == 208) {
                            swal(res.data.message)
                        } 
                        else{
                            swal(res.data.message);
                            $state.go("pages.users.view");
                        }     
                        

                    } else
                       swal(CONSTANTS.serverProblem);
                })
           
        }
        else{
             $scope.$broadcast('show-error-event');
         }
         $scope.isAccountImageSelected=false;
         $scope.isAccountImageValid=false;
    }

//*********************retrieving users ******************
    $scope.getUsers = function(pageNo) {
        getUsers(pageNo);
    }
    
    $scope.getuploadImages = function($files) {
        $scope.isClearImg= false;
        $scope.isImageChange =true;
        $scope.uploadImageRef = $files[0];
        $scope.isAccountImageSelected = true;
        var reader = new FileReader();
        if ($files[0].size > 2048000) {
            $scope.messageForImage=false;
            swal("Image size should be less than 2MB");
            angular.element("input[type='file']").val(null);
            return false;
        } else {
            $scope.imageHide=false;
            $scope.isAccountImageValid=true;
            $scope.exportMsg="";
            reader.onload = $scope.imageIsLoaded;
            reader.readAsDataURL($files[0]);
        }
    };

    $scope.imageIsLoaded = function(e) {
        $scope.$apply(function() {
            $scope.updateImage = e.target.result;
        });
    }

    $scope.clearImg= function($files){
        $scope.isClearImg= true;
        var reader = new FileReader();
        $scope.uploadImageRef = "";
        reader.onload = "";
        $scope.updateImage=""
        $scope.isImageChange =true;
        var blob=new Blob();
        reader.readAsDataURL(blob);
        angular.element("input[type='file']").val(null);
        $scope.isAccountImageSelected=false;
        $scope.isAccountImageValid=false;
    }
//********************Updating User******************

    $scope.editUser = function(id, data) {
        $scope.topView_Users.push(data.username);
        $scope.isImageChange =false;
        $scope.topView.push("Edit User");
        $state.go("pages.users.edit");
        $scope.idEditUser = id;
        $scope.editVal= data;
        $scope.updateImage = $scope.imgUrldata + data.imageurl;
    }
    
    $scope.editUserForm = function(data,isValid) {
        var userEditData = data;
        var loginadmin = localStorageService.get("loginUser");
        userEditData['type'] = 2;
        userEditData['admingroup'] = loginadmin;
        var url = CONSTANTS.UpdateUsers + "/" + $scope.idEditUser;
        var headers = restServices.getHeadersForPost();
        var config = {};
        config.headers = headers;
        if(isValid){
                $scope.formdata = new FormData();
                $scope.formdata.append('img', $scope.uploadImageRef);
                $scope.formdata.append('data', JSON.stringify(userEditData));
                restServices.restPutType(url, $scope.formdata, config, function(status, res) {
                    if (status) {
                        if (res.status == 208) {
                            swal(CONSTANTS.serverProblem);
                        } else {
                            $scope.uploadImageRef= null;
                            $state.go("pages.users.view");
                           swal(res.data.message);
                        }
                    } else
                        swal(CONSTANTS.serverProblem); 
                });
            }
             $scope.$broadcast('show-error-event');
    }

    
//****************user deletion**********************
    $scope.delete_User = function(data, index) {
        var delUserName= data.username;
        var userId= data._id;
        swal({
                title: "Are you sure?",
                text: "Want to delete "+delUserName,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Delete",
                cancelButtonText: "Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    var url = CONSTANTS.deleteUser + "/" + userId;
                    var headers = restServices.getHeadersForPost();
                    var config = {};
                    config.headers = headers;
                    restServices.restDeleteType(url, config, function(status, res) {
                        if (res.status == 200) 
                          { getUsers();
                            swal("Deleted!", "User successfully deleted.", "success");
                           
                        }
                    });
                } else {
                    swal("Cancelled", "User deletion terminated", "error");
                }
            });
    }

 function getUsers(newPageNumber){
        if($scope.topView_Users.length>2)
            $scope.topView_Users.splice(2,1);
        $scope.usersDiv= false;
        var userData = [];
        var listOfUsers= [];
        var groupname = localStorageService.get("GroupName");
        
        var isLight= localStorageService.get("isLightView");
        if(isLight)
            $scope.isImagelight= true;
        else
            $scope.isImagelight= false;

        var itemsPerPage = 15; //this could be a dynamic value from a drop down
        var url = CONSTANTS.getUsers + "/" +groupname+"/"+itemsPerPage+"/"+newPageNumber;
        var headers = restServices.getHeaders();
        var config = {};
        config.headers = headers;
            restServices.restGetType(url, config, function(status, res) {
                $scope.total_count = res.data.total; // total data count.
                $scope.limit = res.data.limit; // Total Limit to show
                angular.forEach(res.data.docs, function(v, k) {
                    userData.push(v);
                });
                $scope.usersList = userData;
                if($scope.usersList[0] == "No Data Found")
                {
                    $scope.usersDiv= true;
                }
                angular.forEach($scope.usersList, function(v, k) {
                  listOfUsers.push(v.username);
                });
                localStorageService.set("usersList", listOfUsers);
            });
    }


}]);
