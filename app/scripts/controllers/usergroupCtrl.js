'use strict';
angular.module('formszWebApp').controller('usergroupCtrl', ['$rootScope', '$uibModal', '$scope', '$window', '$state', '$http', '$timeout', 'restServices', 'CONSTANTS', 'localStorageService', function($rootScope, $uibModal, $scope, $window, $state, $http, $timeout, restServices, CONSTANTS, localStorageService) {
//*********************Deleting Group*******************
    $scope.phoneValidation="^[0-9]{10}$";
    $scope.defaultPage =1; // initialize page no to 1
    $scope.maxSize = 5;
    $scope.searchBarHide =true;
    $scope.closeFilter=function(){
        $scope.searchBy={};
        $scope.cssHeader= false;
        $scope.searchBarHide= true;
    }
    $scope.openFilter = function(){
        $scope.searchBy= null;
        $scope.searchBarHide= false;
        $scope.cssHeader= true;
    }
    $scope.delete_Usergroup = function(data, index) {
        var delGroupName = data.name;
        $scope.usergroupid = data._id;
        swal({
                title: "Are you sure?",
                text: "Want to delete " + delGroupName,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Delete",
                cancelButtonText: "Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    var url = CONSTANTS.deleteUserGroup + "/" + $scope.usergroupid;
                    var headers = restServices.getHeadersForPost();
                    var config = {};
                    config.headers = headers;
                    restServices.restDeleteType(url, config, function(status, res) {
                        if (res.status == 200) {
                            swal("Deleted!", "Group Successfully deleted.", "success");
                            $state.reload("pages.usergroups");
                        }
                    });
                } else {
                    swal("Cancelled", "Action terminated :)", "error");
                }
            });
    }

    $scope.create_userGroup = function(animation, size, backdrop, keyboard) {
            var modalInstance = $uibModal.open({
                animation: animation,
                templateUrl: 'views/createUserGroupModal.html',
                controller: function($scope, $uibModalInstance) {
                    $scope.cancel = function() {
                        $uibModalInstance.dismiss('cancel');
                    };
                    var adminlist = [];
                    var headers = restServices.getHeaders();
                    var config = {};
                    config.headers = headers;
                    var urlMapped = CONSTANTS.getUnMappedAdminList;
                    restServices.restGetType(urlMapped, config, function(status, res) {
                        var adminlistVal = res.data;
                        angular.forEach(adminlistVal, function(v, k) {
                            adminlist.push(v.username);
                        });
                        if (adminlist[0] == undefined) {
                            $scope.Admindropdown = true;
                            $scope.adminOptionMessage = "No Admin Available";
                        } else {
                            $scope.Admindropdown = false;
                            $scope.adminOptionMessage = "select Type";
                            $scope.adminUMList = adminlist;
                        }

                    });
                    $scope.createGroup = function(data, isValid) {
                        var groupdata = data;
                        var headers = restServices.getHeadersForPost();
                        var config = {};
                        config.headers = headers;
                        if (isValid) {
                            $scope.formdata = new FormData();
                            $scope.formdata.append('data', JSON.stringify(groupdata));
                            var url = CONSTANTS.createGroup;
                            restServices.restPostType(url, $scope.formdata, config, function(status, res) {
                                if (status) {
                                    if (res.data.status == 208) {
                                        swal(res.data.message);
                                    } else if(res.data.status == 200) {
                                        swal(res.data.message);
                                        $uibModalInstance.close();
                                        $state.reload("pages.usergroups");
                                    }
                                } else
                                    swal(CONSTANTS.serverProblem);
                            })
                        } else {
                            $scope.$broadcast('show-error-event');
                        }
                    }
                },
                size: size,
                backdrop: backdrop,
                keyboard: keyboard,
            });
        }
//*********************Retrieving Group Values***************

         $scope.getUserGroups = function(newPageNumber) {
            var groupData = [];
            var grouplistData = [];
            var itemsPerPage = 15;
            var url = CONSTANTS.getUserGroup+"/"+itemsPerPage+"/"+newPageNumber;
            var headers = restServices.getHeaders();
            var config = {};
            config.headers = headers;
            restServices.restGetType(url, config, function(status, res) {
                angular.forEach(res.data.docs, function(v, k) {
                    groupData.push(v);
                });
                $scope.groupList = groupData;
                $scope.total_count = res.data.total; // total data count.
                $scope.limit = res.data.limit; // Total Limit to show
                angular.forEach($scope.groupList, function(v, k) {
                    grouplistData.push(v.name);
                });
            });
        }


    $scope.edit_Usergroup = function(data, id, animation) {
        $rootScope.adminlistdropdown = false;
        $rootScope.adminListUnMapped=[];
        $rootScope.groupEditdata={};
        var url = CONSTANTS.getAdminListForUserGroup + "/" + id;
        var headers = restServices.getHeaders();
        var config = {};
        config.headers = headers;
        restServices.restGetType(url, config, function(status, res) {
            $rootScope.groupEditdata = res.data;
            $rootScope.oldAdmin = res.data.Admin;
            $rootScope.adminListUnMapped =res.data.adminlist;
            if (res.data.Admin == null && res.data.adminlist.length == 0) {
                    $rootScope.errorMsg = "No Admin assigned/ No Admin data available";
                    $rootScope.adminlistdropdown = true;
            }
            else if (res.data.adminlist.length>0 && res.data.Admin == null){
                    $rootScope.errorMsg = "Select Admin";
                    $rootScope.defaultMsg= true;
                    if(!res.data.Admin == null)
                        $rootScope.adminListUnMapped.push(res.data.Admin);
            } 
            else {
                $rootScope.errorMsg = res.data.Admin;
                $rootScope.defaultMsg= true;
                $rootScope.adminlistdropdown = false;
            }
        });
        localStorageService.set("editGroupId", id);
        localStorageService.set("editGroupName", data.name);
        var modalInstance = $uibModal.open({
            animation: animation,
            templateUrl: 'views/EditUserGroupModal.html',
            controller: function($scope, $uibModalInstance) {
                $scope.editName = localStorageService.get("editGroupName");
                $scope.cancel = function() {
                    $uibModalInstance.dismiss('cancel');
                };
                $scope.editGroup = function(data,isValid) {
                    var EditId = localStorageService.get("editGroupId");
                    var groupEditdata = data;
                    if($rootScope.oldAdmin!==data.Admin)
                    groupEditdata.oldAdmin = $rootScope.oldAdmin;
                    else
                    groupEditdata.oldAdmin= null;
                    var headers = restServices.getHeadersForPost();
                    var config = {};
                    config.headers = headers;
                    if(isValid){
                    $scope.formdata = new FormData();
                    $scope.formdata.append('data', JSON.stringify(groupEditdata));
                    var url = CONSTANTS.Updategroup + "/" + EditId;
                    if(groupEditdata.Admin == "No admin assigned/No Unmaped admins" || groupEditdata.Admin =="Select Admin")
                    {
                        $rootScope.adminListUnMapped.pop(groupEditdata.Admin);
                        groupEditdata.Admin= null;
                        $rootScope.adminListUnMapped = null;
                        groupEditdata.oldAdmin = null;
                    }
                    restServices.restPutType(url, $scope.formdata, config, function(status, res) {
                        if (status) {
                            if (res.status == 208) {
                              swal(CONSTANTS.serverProblem);
                            } else {
                               swal(res.data.message);
                                $uibModalInstance.close();
                                $state.reload("pages.usergroups");
                               
                            }
                        } else
                            swal(CONSTANTS.serverProblem);
                    })
                    $scope.$broadcast('show-error-event');

                }
            }
            }

        })
    }
    
}]);