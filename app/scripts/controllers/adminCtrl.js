'use strict';
var app = angular.module('formszWebApp').
controller('adminCtrl', ['$rootScope', '$uibModal', '$scope', '$window', '$state', '$http', '$timeout', 'CONSTANTS', 'restServices', 'localStorageService', function($rootScope, $uibModal, $scope, $window, $state, $http, $timeout, CONSTANTS, rs, localStorageService) {
    $scope.isClearImg= true;
    $scope.searchBarHide= true;
    $scope.uploadImageRef = null;
    $scope.defaultPage =1; // initialize page no to 1
    $scope.maxSize = 5;
    $scope.topView_Admin = ["Home", "Admin"];
    $scope.closeFilter=function(){
        $scope.searchBy={};
        $scope.cssHeader= false;
        $scope.searchBarHide= true;
    }

    $scope.openFilter = function(){
        $scope.searchBarHide= false;
        $scope.cssHeader= true;
        $scope.searchBy= undefined;
    }
    $scope.topViewAdmin = function() {
        if ($scope.topView_Admin.length>2) {
            $scope.topView_Admin.splice(2, 1);
            $state.go("pages.admin.view");
        }
    }

    // @Author:Phani Kumar Narina
    //Admin deletion
    $scope.editAdminData = {};
    $scope.uploadImageRef = null;
    $scope.imgUrldataAdmin = CONSTANTS.Golobal_imagePath;
    $scope.errSrc = "images/nopreview.jpg";
    $scope.topView = ["Home", "Admin"];
    $scope.isImageChange =false;
    $scope.isDisableadmindropdown = false;
    $scope.phoneValidation="^[0-9]{10}$";
    $rootScope.topViewAdmin = function() {
        if ($scope.topView.length > 2) {
            $scope.topView.splice(2, 1);
            $state.go("pages.admin.view");
        }
    }
    $scope.delete_Admin = function(data, index) {
            var adminDelName = data.username;
            var adminDelId = data._id;
            swal({
                    title: "Are you sure?",
                    text: "Want to Delete " + adminDelName,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Delete",
                    cancelButtonText: "Cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        var url = CONSTANTS.deleteAdmin + "/" + adminDelId;
                        var headers = rs.getHeadersForPost();
                        var config = {};
                        config.headers = headers;
                        rs.restDeleteType(url, config, function(status, res) {
                            if (res.status == 200) {
                                swal("Deleted!", "Admin Successfully deleted.", "success");
                               $state.reload("pages.admin.view");
                            } /*$state.reload("pages.admin.view");*/
                        });
                    } else {
                        swal("Cancelled", "Admin deletionn terminated :)", "error");
                    }
                });
        }
        //************** Retrieving unmapped users data************
    $scope.create_Admin = function() {
            $scope.topView_Admin.push("Create Admin"); 
            $scope.updateImage = "";
            $state.go('pages.admin.create');
            var grouplist = [];
            var headers = rs.getHeaders();
            var config = {};
            config.headers = headers;
            var urlMapped = CONSTANTS.getUnMappedGroup;
            rs.restGetType(urlMapped, config, function(status, res) {
                var grouplistVal = res.data;
                angular.forEach(grouplistVal, function(v, k) {
                    grouplist.push(v.name);
                });
                if (grouplist[0] == undefined) {
                    $scope.Groupdropdown = true;
                    $scope.optionMessage = "No Group Available";
                } else {
                    $scope.Groupdropdown = false;
                    $scope.optionMessage = "select Type";
                    $scope.GroupList = grouplist;
                }

            });
        }
        //*******************Creating Admin****************
    $scope.createAdmin = function(user, isValid) {
        var adminData = user;
        adminData['type'] = 1;
        adminData['admingroup'] = adminData.username;
        var headers = rs.getHeadersForPost();
        var config = {};
        config.headers = headers;
        if (isValid) {
            $scope.formdata = new FormData();
            
            if( !$scope.isAccountImageSelected){
                $scope.uploadImageRef == null;
            }
            else if($scope.isAccountImageSelected && !$scope.isAccountImageValid){
                swal("Invalid Image, please choose correct Image");
                return;
            }
            $scope.formdata.append('img', $scope.uploadImageRef);
            $scope.formdata.append('data', JSON.stringify(adminData));
            var url = CONSTANTS.createAdmin;
            rs.restPostType(url, $scope.formdata, config, function(status, res) {
                if (status) {
                    if (res.status == 208) {
                        swal(CONSTANTS.serverProblem);
                    } else {
                        //$scope.uploadImageRef = "";
                         if(res.data.message== "Admin already exits")
                         {
                            $state.go("pages.admin.create"); 
                            swal(res.data.message); 
                         } 
                         else{
                            swal(res.data.message);
                            $state.go("pages.admin.view") 
                          
                        }          
                    }

                } else
                    swal(CONSTANTS.serverProblem);
            });
        } else {
            $scope.$broadcast('show-error-event');

        }
        $scope.isAccountImageSelected=false;
        $scope.isAccountImageValid=false;
    }

    $scope.editAdmin = function(id, data) {
        $scope.admindropdown = false;
        $scope.groupList = [];
        $scope.topView_Admin.push(data.username);
        $scope.isImageChange =false;
        $scope.idEditAdmin = id;
        $scope.editAdminData = {};
        var unmapped = [];
        $scope.updateImage = $scope.imgUrldataAdmin + data.imageurl;
        var url = CONSTANTS.getUserGroupForAdmin + "/" + $scope.idEditAdmin;
        var headers = rs.getHeaders();
        var config = {};
        config.headers = headers;
        rs.restGetType(url, config, function(status, res) {
            $scope.editAdminData = res.data;
            $scope.groupList = res.data.grouplist;
            $scope.oldAdmin = res.data.groupname;
            if (res.data.groupname == null && res.data.grouplist.length == 0) {
                    $scope.errorImg = "No Group assigned/ No group data available";
                    $scope.isDisableadmindropdown = true;
             }
            else if (res.data.grouplist.length>0 && res.data.groupname == null){
                    $scope.errorImg = "Select Group";
                    $scope.defaultMsg= true;
                    if(!res.data.groupname == null)
                        $scope.groupList.push(res.data.groupname);
            } 
            else {
                    $scope.errorImg = res.data.groupname;
                    $scope.defaultMsg= true;
                    $scope.isDisableadmindropdown = false;
                    /*$scope.groupList.push(res.data.groupname);*/
            }

            $state.go("pages.admin.edit");

        });
    }
//****************Update Admin****************$scope.adminName*
    $scope.editAdminForm = function(data,isValid) {
            var adminEditData = data;        
            adminEditData.oldGroupname = $scope.oldAdmin;
            adminEditData['type'] = 1;
            adminEditData['admingroup'] = $scope.editAdminData.groupname;
            var url = CONSTANTS.UpdateAdmin + "/" +$scope.idEditAdmin;
            var headers = rs.getHeadersForPost();
            var config = {};
            config.headers = headers;
               if (isValid) {

                $scope.formdata = new FormData();
                $scope.formdata = new FormData();
                $scope.formdata.append('img', $scope.uploadImageRef);
                $scope.formdata.append('data', JSON.stringify(adminEditData));
                rs.restPutType(url, $scope.formdata, config, function(status, res) {
                    if (status) {
                        if (res.status == 208) {
                            swal(" Already Reported");
                        } else {
                            $scope.uploadImageRef=null;
                            swal(res.data.message);
                            $state.go("pages.admin.view");
                        }

                    } else
                        swal(CONSTANTS.serverProblem);
                });
            }
          $scope.$broadcast('show-error-event');
          
        }
//*****************Retreiving Admin data*****************

     $scope.getAdmins = function(newPageNumber) {
        var itemsPerPage = 15; //this could be a dynamic value from a drop down
        var adminData = [];
        var url = CONSTANTS.getAdmin+"/"+itemsPerPage+"/"+newPageNumber;
        var headers = rs.getHeaders();
        var config = {};
        config.headers = headers;
        var isLight= localStorageService.get("isLightView");
        if(isLight)
            $scope.isImagelight= true;
        else
            $scope.isImagelight= false;
        if ($scope.topView_Admin.length>2) {
            $scope.topView_Admin.splice(2, 1);
        }

        if ($scope.usersList == undefined) {
            rs.restGetType(url, config, function(status, res) {
                angular.forEach(res.data.docs, function(v, k) {
                    adminData.push(v);
                });
                $scope.adminDataList = adminData;
                $scope.total_count = res.data.total; // total data count.
                $scope.limit = res.data.limit; // Total Limit to show
                localStorageService.set("adminlist", $scope.adminDataList);
            });
        }
    }

$scope.getuploadImages = function($files) {
        $scope.isClearImg= false;
        $scope.isImageChange =true;
        $scope.uploadImageRef = $files[0];
        $scope.isAccountImageSelected = true;
        var reader = new FileReader();
        if ($files[0].size > 2048000) {
            $scope.messageForImage=false;
            swal("Image size should be less than 2MB");
            angular.element("input[type='file']").val(null);
            $scope.isAccountImageValid=false;
            return false;
        } else {
            $scope.isAccountImageValid=true;
            reader.onload = $scope.imageIsLoaded;
            reader.readAsDataURL($files[0]);
        }
    };
    
    $scope.clearImg= function($files){
        $scope.isClearImg= true;
        var reader = new FileReader();
        $scope.uploadImageRef = "";
        reader.onload = "";
        $scope.updateImage="";
        $scope.isImageChange =true;
        var blob=new Blob();
        reader.readAsDataURL(blob);
        angular.element("input[type='file']").val(null);
        $scope.isAccountImageSelected=false;
        $scope.isAccountImageValid=false;
    }

    $scope.imageIsLoaded = function(e) {
        $scope.$apply(function() {
            $scope.updateImage = e.target.result;
        });
    }
}]);

