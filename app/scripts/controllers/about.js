'use strict';

/**
 * @ngdoc function
 * @name formszWebApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the formszWebApp
 */
angular.module('formszWebApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
