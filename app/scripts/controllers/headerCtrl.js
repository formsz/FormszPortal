'use strict';
var app = angular.module('formszWebApp').
controller('headerCtrl', ['$rootScope','$scope', '$window', '$state', '$http', '$timeout', 'CONSTANTS', 'restServices', 'localStorageService','$uibModal', function($rootScope,$scope, $window, $state, $http,$timeout, CONSTANTS, restServices, localStorageService,$uibModal) {

    $scope.headerName=localStorageService.get("userdata").username;
    $scope.type = localStorageService.get("userdata").type;
    if ($scope.type == 0) 
    {
        $scope.adminImage = "images/latest_logo.png";
    }
    else
    $scope.adminImage= CONSTANTS.Golobal_imagePath+localStorageService.get("userdata").imageurl;
    $scope.errSrc = "images/latest_logo.png";
    $scope.animationsEnabled = true;


    // Change Admin Password
    $scope.changePwd = function(size) {

        $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'views/adminPwdChange.html',
            size: size,
            backdrop: false, 
            keyboard: true,     
            controller: function($scope,$uibModalInstance){
                $scope.dialogClose = function(){
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.newPassword = function(data,isValid){
                if(isValid){
                    var username = localStorageService.get("userdata").username;
                    var parameters = "{\"oldpassword\":\""+data.cpswd+"\",\"newpassword\":\""+data.rtnewpwd+"\",\"username\":\""+username+"\"}";
                    var headers = restServices.getHeaders();
                    var config = {};
                    config.headers = headers;
                    var url = CONSTANTS.pwdChange;
                    restServices.restPostType(url,parameters,config,function(status, data) {
                        if(status)
                        {
                            if (data.data.status == 200) {
                                $uibModalInstance.dismiss();
                                swal(data.data.message);
                            }
                            else if(data.data.status == 204){
                                swal(data.data.message);
                            }
                            else{
                                swal(CONSTANTS.serverProblem);
                            }
                        }
                        else
                            swal(CONSTANTS.serverProblem);
                    });
                }
                else
                {
                  $scope.$broadcast('show-error-event');
                }
            }
        }
        });
    }


    //Fullscreen View
    this.fullScreen = function() {
        //Launch
        function launchIntoFullscreen(element) {
            if(element.requestFullscreen) {
                element.requestFullscreen();
            } else if(element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if(element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            } else if(element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }
        }

        //Exit
        function exitFullscreen() {
            if(document.exitFullscreen) {
                document.exitFullscreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
        }

        if (exitFullscreen()) {
            launchIntoFullscreen(document.documentElement);
        }
        else {
            launchIntoFullscreen(document.documentElement);
        }
    }
}]);
