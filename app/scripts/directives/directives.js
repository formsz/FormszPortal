var app = angular.module('formszWebApp.directives', []);



app.directive("ngFiles",['$parse',function($parse){
  
  function getChangedImage_link (scope, element, attrs)
  { 
    var onChange = $parse(attrs.ngFiles);
    element.on('change', function (event) {
                    onChange(scope, { $files: event.target.files });
              });

  }

  return{
         link: getChangedImage_link
  }

}])

app.directive('validFile',function(){
  return {
    require:'ngModel',
    link:function(scope,el,attrs,ngModel){
      //change event is fired when file is selected
      el.bind('change',function(){
        scope.$apply(function(){
          ngModel.$setViewValue(el.val());
          ngModel.$render();
        })
      })
    }
  }
})


app.directive('capitalizeFirst', function($parse) {
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
           if (inputValue === undefined) { inputValue = ''; }
           var capitalized = inputValue.charAt(0).toUpperCase() +
                             inputValue.substring(1);
           if(capitalized !== inputValue) {
              modelCtrl.$setViewValue(capitalized);
              modelCtrl.$render();
            }         
            return capitalized;
         }
         modelCtrl.$parsers.push(capitalize);
         capitalize($parse(attrs.ngModel)(scope)); // capitalize initial value
     }
   };
})

// Pattern to check [ALPHA ONLY bjbkjb]/ [NUMBER ONLY 555555]/[ALPHANUMERIC ONLY] /[WHITESPACE CHARACTERS ONLY] /[ABCDEFG ONLY] 
app.directive('allowPattern', function allowPatternDirective() {
    return {
        restrict: "A",
        compile: function(tElement, tAttrs) {
          return function(scope, element, attrs) {
          // I handle key events
          element.bind("keypress", function(event) {
          var keyCode = event.which || event.keyCode; // I safely get the keyCode pressed from the event.
          var keyCodeChar = String.fromCharCode(keyCode); // I determine the char from the keyCode.
          
          // If the keyCode char does not match the allowed Regex Pattern, then don't allow the input into the field.
                  if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
                  event.preventDefault();
                      return false;
                  }
                });
            };
        }
    };
})

app.directive('onlyDigits', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var digits = val.replace(/[^0-9]/g, '');

            if (digits !== val) {
              ctrl.$setViewValue(digits);
              ctrl.$render();
            }
            return parseInt(digits,10);
          }
          return undefined;
        }            
        ctrl.$parsers.push(inputValue);
      }
    };
})


app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});





app.directive('dExpandCollapse', function() {

  return {
          restrict: 'EA',
          link: function(scope, element, attrs){
            
            $(element).click( function() {
              //var show = "false";
              $(element).find(".answer").slideToggle('200',function() {            
                    // You may toggle + - icon     
                $(element).find("span").toggleClass('pform faqMinus');
              });

              
              if($("div.answer:visible").length>1) { 
  // You may toggle + - icon                  //$(this).parent().find("span.faqMinus").removeClass('faqMinus').addClass('faqPlus');
                $(this).siblings().find(".answer").slideUp('slow'); 
              }
              

            });

          }
        }



});

app.directive('textBox',function(){
  return{
    restrict:"AE",
    replace:false,
    scope:{
      setProperties:'&',
      deleteWidget:'&'
    },
    template:"<div class='card-header ch-alt'>"+
                  "<h2><i class='fa fa-dot-circle-o fa-1x'></i>TEXTBOX</h2>"+
                    "<ul class='actions'>"+
                      "<li>"+
                        "<a href=''  id='setTextboxproperties' >"+
                          "<i class='zmdi zmdi-caret-down-circle'></i>"+
                        "</a>"+
                      "</li>"+
                      "<li>"+
                        "<a href='' id='deleteWidget' ><i class='zmdi zmdi-delete'></i></a>"+
                      "</li>"+ 
                    "</ul>"+
                "</div>",
    link:function(scope,elem,attr){
      angular.element('#setTextboxproperties').bind("click", function() {
      scope.setProperties();
      })

      angular.element('#deleteWidget').bind("click", function() {
      scope.deleteWidget();
      })

    
  }

  }
})

app.directive("showErrors",function($timeout){
  return{
    restrict:'A',
    require:'^form',
    link:function(scope,el,attrs,formCtrl){
      var inputEl   = el[0].querySelector("[name]");
      var inputNgE1 = angular.element(inputEl);
      var inputname =inputNgE1.attr('name');
      var helpText  = angular.element(el[0].querySelector("help-block"));
      inputNgE1.bind('change',function(){
        el.toggleClass('has-error',formCtrl[inputname].$invalid);
        //helpText.toggleClass('hide',formCtrl[inputname].$valid );
      })

      scope.$on('show-error-event',function(){
        el.toggleClass('has-error',formCtrl[inputname].$invalid);
      })
      scope.$on('hide-error-event',function(){
        $timeout(function(){
          el.removeClass('has-error');
        },0,false)
      })
    }
  }

});

 app.directive("compareTo",[function(){
   return{
    require:"ngModel",
    scope:{
      passwords:"=compareTo"
    },
    link:function(scope,element,attributes, ctrl){
      scope.$watch("passwords[1]",function(){
        var v = scope.passwords[0]===scope.passwords[1];
        ctrl.$setValidity('pwmatch', v);
      })
    }
  
   }
 }]);
 
app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

// PK Code to verify password 
app.directive('nxEqualEx', function() {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, model) {
            if (!attrs.nxEqualEx) {
                console.error('nxEqualEx expects a model as an argument!');
                return;
            }
            scope.$watch(attrs.nxEqualEx, function (value) {
                // Only compare values if the second ctrl has a value.
                if (model.$viewValue !== undefined && model.$viewValue !== '') {
                    model.$setValidity('nxEqualEx', value === model.$viewValue);
                }
            });
            model.$parsers.push(function (value) {
                // Mute the nxEqual error if the second ctrl is empty.
                if (value === undefined || value === '') {
                    model.$setValidity('nxEqualEx', true);
                    return value;
                }
                var isValid = value === scope.$eval(attrs.nxEqualEx);
                model.$setValidity('nxEqualEx', isValid);
                return isValid ? value : undefined;
            });
        }
    };
});

// Directive for image Break
app.directive('errSrc', function() {
   return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
      
      attrs.$observe('ngSrc', function(value) {
        if (!value && attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
    }
  }
});
app.directive('focusMe', function($timeout) {
  return {
    scope: { trigger: '=focusMe' },
    link: function(scope, element) {
      scope.$watch('trigger', function(value) {
        if(value === true) { 
          //console.log('trigger',value);
          //$timeout(function() {
            element[0].focus();
            scope.trigger = false;
          //});
        }
      });
    }
  };
});

app.directive('multiselectDropdown', function () {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            options: '=',
        },
        template:
            "<div class='btn-group' data-ng-class='{open: open1}' style='width: 150px;height:30px'>" +
            "<div class='btn btn-small' style='width: 10px,height:30px;' ng-click='openDropdowntest()'>Select Users</div>" +
            
            "<ul class='dropdown-menu' aria-labelledby='dropdownMenu' '>" +
            "<li style='cursor:pointer;' data-ng-repeat='option in options'><a data-ng-click='toggleSelectItem(option)'><span data-ng-class='getClassName(option)' aria-hidden='true'></span> {{option.name}}</a></li>" +
            "</ul>" +
            "</div>",

        controller: function ($scope) {
            $scope.openDropdowntest = function () {
                $scope.open1 = !$scope.open1;
            };

            $scope.selectAll = function () {
                $scope.model = [];
                angular.forEach($scope.options, function (item, index) {
                    $scope.model.push(item);
                });
            };

            $scope.deselectAll = function () {
                $scope.model = [];
            };

            $scope.toggleSelectItem = function (option) {
                var intIndex = -1;
                angular.forEach($scope.model, function (item, index) {
                    if (item.name == option.name) {
                        intIndex = index;
                    }
                });

                if (intIndex >= 0) {
                    $scope.model.splice(intIndex, 1);
                } else {
                    $scope.model.push(option);
                }
            };

            $scope.getClassName = function (option) {
                var varClassName = 'glyphicon glyphicon-remove-circle';
                angular.forEach($scope.model, function (item, index) {
                    if (item.name == option.name) {
                        varClassName = 'glyphicon glyphicon-ok-circle';
                    }
                });
                return (varClassName);
            };
        }
    }
});